﻿using System;
using System.Collections.Generic;


namespace InterviewExercise.model
{
    public class CurrencyExchange
    {
        public IDictionary<String, double> rates { get; set; }
        public string @base { get; set; }
        public DateTime date { get; set; }
    }
}


