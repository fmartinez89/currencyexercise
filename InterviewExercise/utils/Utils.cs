﻿using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace InterviewExercise.utils
{
    public static class Utils
    {
        /// <summary>
        /// Check if each key in the dictionary
        /// match with a element in the list. If
        /// there are additonal elemtns return false also
        /// </summary>
        /// <param name="dic">Generic IDictionary</param> 
        /// <param name="list">Generic List</param> 
        public static bool VerifyIfElementsMatch<T, K>(IDictionary<T, K> dict, List<T> list)
        {
            ICollection<T> keys = dict.Keys;
            bool isEqual = Enumerable.SequenceEqual(keys.OrderBy(e => e), list.OrderBy(e => e));
            return isEqual;
        }

        /// <summary>
        /// Convert a specflow table into a string List
        /// </summary>
        /// <param name="table">Specflow table</param> 
        ///<param name="list">List of strings</param> 
        /// <param name="tableName">Name of the table to be transaformed</param> 
        public static List<string> TableToList(Table table, List<string> list, string tableName)
        {
            foreach (var tableRow in table.Rows)
            {
                list.Add(tableRow[tableName]);
            }
            return list;
        }
    }
}
