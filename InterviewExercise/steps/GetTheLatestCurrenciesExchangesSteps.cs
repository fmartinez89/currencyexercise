﻿using InterviewExercise.api;
using InterviewExercise.model;
using InterviewExercise.utils;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace InterviewExercise.steps
{
    [Binding]
    public class GetTheLatestCurrenciesExchangesSteps
    {
        private IRestResponse response;
        ScenarioContext scenarioContext;
        ExchangeRatesAPI currencyExchangeAPI;

        public GetTheLatestCurrenciesExchangesSteps(ScenarioContext scenarioContext, ExchangeRatesAPI currencyExchangeAPI)
        {
            this.scenarioContext = scenarioContext;
            this.currencyExchangeAPI = currencyExchangeAPI;
        }
        [Given(@"The user uses '(.*)' currency as base")]
        public void GivenTheUserUsesCurrencyAsBase(string baseCurrency)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("base", baseCurrency);
            scenarioContext.Add("paramters", parameters);
        }

        [Given(@"the user sets the desired exchange rates")]
        public void GivenTheUserSetsTheDesiredExchangeRates(Table desiredRatesTable)
        {
            var parameters = scenarioContext.Get<IDictionary<string, object>>("paramters");
            List<string> desiredRates = new List<string>();
            desiredRates = Utils.TableToList(desiredRatesTable, desiredRates, "specificRates");
            var symbols = String.Join(",", new List<string>(desiredRates).ConvertAll(i => i.ToString()).ToArray());
            parameters.Add("symbols", symbols);
        }

        [When(@"The user requests for all exchange reference rates")]
        public void WhenTheUserRequestsForAllExchangeReferenceRates()
        {
            var parameters = scenarioContext.Get<IDictionary<string, object>>("paramters");
            response = new RestResponse();
            response = currencyExchangeAPI.GetLatestExchangeRates(parameters);
            //Save the entire response in the scenario context
            scenarioContext.Add("response", response);
            //Save the content response as object in the scenario context
            CurrencyExchange currencyExchange = currencyExchangeAPI.ResponseToObject<CurrencyExchange>(response);
            scenarioContext.Add("currencyExchange", currencyExchange);

        }

        [Then(@"the following currencies rates are displayed")]
        public void ThenTheFollowingCurrenciesRatesAreDisplayed(Table ratesTable)
        {
            List<string> expectedRates = new List<string>();
            expectedRates = Utils.TableToList(ratesTable, expectedRates, "rates");
            response = scenarioContext.Get<IRestResponse>("response");
            var currencyExchange = scenarioContext.Get<CurrencyExchange>("currencyExchange");
            IDictionary<string, double> rates = currencyExchange.rates;
            Assert.True(Utils.VerifyIfElementsMatch(rates, expectedRates), "One or more currencies are not in the response");    
        }

        [Then(@"currency rates list is not empty")]
        public void ThenCurrencyRatesListIsNotEmpty()
        {
            var currencyExchange = scenarioContext.Get<CurrencyExchange>("currencyExchange");
            IDictionary<string, double> rates = currencyExchange.rates;
            scenarioContext.Add("rates", rates);           
            Assert.IsNotEmpty(rates, "Rates are not displayed");
        }

        [Then(@"the status code is (.*)")]
        public void ThenTheStatusCodeIs(int expectedStatusCode)
        {
            response = scenarioContext.Get<IRestResponse>("response");
            Assert.AreEqual(expectedStatusCode, (int)response.StatusCode);
        }

        [Then(@"the base currency '(.*)' should not be returned in the rates list")]
        public void ThenTheBaseCurrencyShouldNotBeReturnedInTheRatesList(string baseCurrency)
        { 
            var currencyExchange = scenarioContext.Get<CurrencyExchange>("currencyExchange");
            IDictionary<string, double> rates = currencyExchange.rates;
            Assert.True(!rates.ContainsKey(baseCurrency), "The base currency is being displayed in the rates list");
        }

        [Then(@"'(.*)' is displayed as a base")]
        public void ThenIsDisplayedAsABase(string baseCurrency)
        {
            var currencyExchange = scenarioContext.Get<CurrencyExchange>("currencyExchange");
            StringAssert.AreEqualIgnoringCase(currencyExchange.@base, baseCurrency, "Base currency is not the expected");
        }
    }
}
