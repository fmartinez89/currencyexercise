﻿Feature: Get the latest currencies exchanges
  As a user
  I want to obtain the current foreign exchange rates

@mytag
#for testing purpose just some rates are being validated
#I assume that the rates can change, that they are not fixed
Scenario: Get all the latest foreign exchange reference rates, using the default base currency EUR
	Given The user uses '' currency as base
	When The user requests for all exchange reference rates
	Then the status code is 200
	And currency rates list is not empty
	And the base currency 'EUR' should not be returned in the rates list
	And 'EUR' is displayed as a base

Scenario: Get all the latest foreign exchange reference rates, USD is used as base currency
	Given The user uses 'USD' currency as base
	When The user requests for all exchange reference rates
	Then the status code is 200
	And currency rates list is not empty
	And 'USD' is displayed as a base

Scenario: Get specific exchange rates by setting the symbols parameter, using default base currency
	Given The user uses '' currency as base
	And the user sets the desired exchange rates
		| specificRates |
		| GBP           |
		| PLN           |
	When The user requests for all exchange reference rates
	Then the status code is 200
	And the following currencies rates are displayed
		| rates |
		| GBP   |
		| PLN   |
	And the base currency 'EUR' should not be returned in the rates list
	And 'EUR' is displayed as a base

Scenario: Get specific exchange rates by setting the symbols parameter, using 'USD' as base currency. Different currency than the default
	Given The user uses 'USD' currency as base
	And the user sets the desired exchange rates
		| specificRates |
		| GBP           |
		| PLN           |
	When The user requests for all exchange reference rates
	Then the status code is 200
	And the following currencies rates are displayed
		| rates |
		| GBP   |
		| PLN   |
	And 'USD' is displayed as a base

Scenario: Get all the latest foreign exchange reference rates, use a non existing currency as base
	Given The user uses 'NonExisting' currency as base
	When The user requests for all exchange reference rates
	Then the status code is 400

Scenario: Get specific exchange rates by setting the symbols parameter, use a non existing currency in the symbols parameter
	Given The user uses 'USD' currency as base
	And the user sets the desired exchange rates
		| specificRates			|
		| NonExisting           |
		| PLN					|
	When The user requests for all exchange reference rates
	Then the status code is 400