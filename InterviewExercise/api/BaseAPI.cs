﻿
using RestSharp;
using System;
using System.Collections.Generic;

namespace InterviewExercise.api
{
    /// <summary>
    /// Class with reusable API methods 
    /// </summary>
    public class BaseAPI
    {
        private RestRequest restRequest;
        private RestClient restClient;

        public BaseAPI()
        {
            restRequest = new RestRequest();
        }


        /// <summary>
        /// <summary>
        /// Method to set the resource path
        /// </summary>
        /// <param name="resource">Path of the resource</param>
        public BaseAPI SetPath(string resource)
        {
            restRequest.Resource = resource;
            return this;
        }

        /// <summary>
        /// Define the HTTP method to use in the request
        /// </summary>
        /// <param name="method">HTTP method</param>
        public BaseAPI SetMethod(Method method)
        {
            restRequest.Method = method;
            return this;
        }

        /// <summary>
        /// Method to add the request headers
        /// </summary>
        /// <param name="headers">Request headers</param>
        public BaseAPI AddHeaders(IDictionary<string, string> headers)
        {
            foreach (var header in headers)
            {
                restRequest.AddParameter(header.Key, header.Value, ParameterType.HttpHeader);
            }
            return this;
        }

        /// <summary>
        /// Method to add the request body
        /// </summary>
        /// <param name="data">Request body as object</param>
        public BaseAPI AddJsonContent(object data)
        {
            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddJsonBody(data);
            return this;
        }

        /// <summary>
        /// Method to add the request parameters
        /// </summary>
        /// <param name="parameters">Request parameters</param>
        public BaseAPI AddParameters(IDictionary<string, object> parameters)
        {
            foreach (var item in parameters)
            {
                restRequest.AddParameter(item.Key, item.Value);
            }
            return this;
        }

        public IRestResponse Execute()
        {
            try
            {
                restClient = new RestClient("https://api.exchangeratesapi.io/");
                var response = restClient.Execute(restRequest);
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
