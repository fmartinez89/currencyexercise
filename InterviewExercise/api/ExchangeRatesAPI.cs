﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace InterviewExercise.api
{
    /// <summary>
    /// Class with methods to perform request in the Exchange rates api
    /// This class adds an additional level of abstraction to hide the
    /// API implementation
    /// </summary>
    public class ExchangeRatesAPI : BaseAPI
    {
        private IRestResponse restResponse;

        /// <summary>
        /// Reusable method to create a resource
        /// </summary>
        /// </summary>
        /// <param name="bodyObject">Body request as object</param> 
        /// <param name="resource">Path of the resource</param> 
        public IRestResponse CreateResource(object bodyObject, string resource)
        {
            var request = new BaseAPI()
                            .SetMethod(Method.POST)
                            .SetPath(resource)
                            .AddJsonContent(bodyObject);
            restResponse = new RestResponse();
            restResponse = request.Execute();
            return restResponse;
        }

        /// <summary>
        /// Reusable method to get the response status code
        /// </summary>
        /// <param name="bodyObject">Body request as object</param> 
        /// <param name="resource">Path of the resource</param>
        public HttpStatusCode GetStatusCode(IRestResponse restResponse)
        {
            return restResponse.StatusCode;
        }

        /// <summary>
        /// Reusable method to deserilize the response content in
        /// the desired type
        /// </summary>
        /// <param name="restResponse">The api response</param> 
        public T ResponseToObject<T>(IRestResponse restResponse)
        {
            var data = JsonConvert.DeserializeObject<T>(restResponse.Content);
            return data;
        }

        /// <summary>
        /// <summary>
        /// Get the latest exchange reates
        /// </summary>
        /// <param name="parameters">Request parameters</param> 
        public IRestResponse GetLatestExchangeRates(IDictionary<string, object> parameters)
        {
            var request = new BaseAPI()
                           .SetMethod(Method.GET)
                           .AddParameters(parameters)
                           .SetPath("latest");
            restResponse = new RestResponse();
            restResponse = request.Execute();
            return restResponse;
        }

    }
}
